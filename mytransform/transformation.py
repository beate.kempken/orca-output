# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 14:10:49 2021

@author: Dohmen
"""

# Import the necessary packages
import sys
import numpy as np
import matplotlib.pyplot as plt
import os

from pathlib import Path

sys.path.insert(0, 'D:\\orca-output')

# add git repository path for orca_coord to path for home computer
sys.path.insert(0, 'S:\\PhD\\orca-output')

# add git repository path for orca_coord to path for laptop computer
sys.path.insert(0, 'C:\\Users\\Dohmen\\projects\\orca-output')

# Import important functions from complementary files
from GeometryClass import Geometry, information_to_excel
from spfitExtension import (read_multiple_linfiles,
                                       search_for_duplicates,
                                       same_qantum_numbers)
from crestExtension import crest_xyz_to_ORCA
from strfitExtension import submit_multipe_stf_files
from GaussianScan import read_Gaussian_scan

files = []
for path in Path(
        'C:\\Users\\Dohmen\\ownCloud\\PhD\\calculations\\halogenbenzaldehyde-scans\\Gaussian\\Bromobenzaldehyde').rglob('*.log'):
    if '2-BrBz' in path.name:
        if '_2' not in path.name:
            if 'HF' not in path.name:
                files.append(path)

ortho_plots = []
ortho_names = []
for file in files:
    ortho_names.append(file.stem)
    plot = read_Gaussian_scan(file)
    ortho_plots.append(plot)


ortho_plots = np.array(ortho_plots)

# %%

files = []
for path in Path(
        'C:\\Users\\Dohmen\\ownCloud\\PhD\\calculations\\halogenbenzaldehyde-scans\\Gaussian\\Bromobenzaldehyde').rglob('*.log'):
    if '3-BrBz' in path.name:
        if '_2' not in path.name:
            if 'HF' not in path.name:
                files.append(path)

meta_plots = []
meta_names = []
for file in files:
    meta_names.append(file.stem)
    plot = read_Gaussian_scan(file)
    meta_plots.append(plot)

meta_plots = np.array(meta_plots)

# %%

files = []
for path in Path(
        'C:\\Users\\Dohmen\\ownCloud\\PhD\\calculations\\halogenbenzaldehyde-scans\\Gaussian\\Bromobenzaldehyde').rglob('*.log'):
    if '4-BrBz' in path.name:
        if '_2' not in path.name:
            if 'HF' not in path.name:
                files.append(path)

para_plots = []
para_names = []
for file in files:
    para_names.append(file.stem)
    plot = read_Gaussian_scan(file)
    para_plots.append(plot)

para_plots = np.array(para_plots)

# %%

mydir = Path(os.path.split(os.path.splitext(file)[0])[0])

plt.subplots(figsize=(15, 5))
plt.title('Absolutle Energy')
ax1 = plt.subplot(1, 4, 1)
for idx, EPlot in enumerate(ortho_plots):
    ax1.plot(EPlot[:, 0], EPlot[:, 1],
             label=ortho_names[idx])
ax1.set_title('ortho-Bromobenzaldehyde')

ax1.set_ylabel('E / Hartree')

ax2 = plt.subplot(1, 4, 2)
for idx, EPlot in enumerate(meta_plots):
    ax2.plot(EPlot[:, 0], EPlot[:, 1],
             label=meta_names[idx])
ax2.set_title('meta-Bromobenzaldehyde')
ax2.set_xlabel('dyhedral angel / degree')


ax3 = plt.subplot(1, 4, 3)
for idx, EPlot in enumerate(para_plots):
    ax3.plot(EPlot[:, 0], EPlot[:, 1],
             label=para_names[idx])
ax3.set_title('para-Bromobenzaldehyde')


lastSubplot = plt.subplot(1, 4, 4)
lastSubplot.set_frame_on(False)
lastSubplot.get_xaxis().set_visible(False)
lastSubplot.get_yaxis().set_visible(False)
for idx, EPlot in enumerate(para_plots):
    lastSubplot.plot(0, 0, label = para_names[idx])
lastSubplot.set_xlim(1, 2)
lastSubplot.set_ylim(1, 2)
lastSubplot.legend(loc = 'lower right')
plt.tight_layout()

savefile = os.path.join(mydir, 'BrBzA-AbsE.png')
plt.savefig(savefile)

# %%

plt.subplots(figsize=(15, 5))
plt.title('relative Energy')
ax1 = plt.subplot(1, 4, 1)
for idx, EPlot in enumerate(ortho_plots):
    ax1.plot(EPlot[:, 0], EPlot[:, 1] - EPlot[:, 1].min(),
             label=ortho_names[idx])
ax1.set_title('ortho-Bromobenzaldehyde')
ax1.set_ylabel('E / Hartree')

ax2 = plt.subplot(1, 4, 2)
for idx, EPlot in enumerate(meta_plots):
    ax2.plot(EPlot[:, 0], EPlot[:, 1] - EPlot[:, 1].min(),
             label=meta_names[idx])
ax2.set_title('meta-Bromobenzaldehyde')
ax2.set_xlabel('dyhedral angel / degree')

ax3 = plt.subplot(1, 4, 3)
for idx, EPlot in enumerate(para_plots):
    ax3.plot(EPlot[:, 0], EPlot[:, 1] - EPlot[:, 1].min(),
             label=para_names[idx])
ax3.set_title('para-Bromobenzaldehyde')

lastSubplot = plt.subplot(1, 4, 4)
lastSubplot.set_frame_on(False)
lastSubplot.get_xaxis().set_visible(False)
lastSubplot.get_yaxis().set_visible(False)
for idx, EPlot in enumerate(para_plots):
    lastSubplot.plot(0, 0, label = para_names[idx])
lastSubplot.set_xlim(1, 2)
lastSubplot.set_ylim(1, 2)
lastSubplot.legend(loc = 'lower right')
plt.tight_layout()

savefile = os.path.join(mydir, 'BrBzA-RelE.png')
plt.savefig(savefile)