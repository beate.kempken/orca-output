# -*- coding: utf-8 -*-
"""
Created on Fri Nov 19 12:33:30 2021

@author: Dohmen
"""

import shutil
import os
from pathlib import Path
import sys
sys.path.insert(0, 'D:\\orca-output')

# add git repository path for orca_coord to path for home computer
sys.path.insert(0, 'S:\\PhD\\orca-output')
from GeometryClass import Geometry

def remove_substring_from_string(s, substr):
    '''
    find start index in s of substring
    remove it by skipping over it
    '''
    for i in range(len(s) - len(substr) + 1):
        if s[i:i+len(substr)] == substr:
            break
    else:
        # break not hit, so substr not found
        return s

    return s[:i] + s[i+len(substr):]


files = []
for path in Path(
        'D:\\OwnCloud\\Obenchain\\Research\\Denis\\Denis Data\\For Extraction').rglob('*.xyz'):
    if 'B3LYP' in path.name:
        if '_trj' not in path.name:
            if 'Xe' not in path.name:
                if 'Cu' not in path.name:
                    if 'Ag' not in path.name:
                        if 'Au' not in path.name:
                            if 'Kr' not in path.name:
                                if  'Flourobenzene-HCl-B3LYP-D4-Opt-Mass2016-VERYTIGHTSCF-VERYTIGHTOPT-x2c-TZVPPall-2c' not in path.name:
                                    if  'Flourobenzene-HCl-B3LYP-D3-Opt-Mass2016-VERYTIGHTSCF-VERYTIGHTOPT-x2c-TZVPPall-2c' not in path.name:
                                        if 'def2-TZVPPD' in path.name:
                                            files.append(path)
                                        if 'x2c-TZVPPall-2c' in path.name:
                                            files.append(path)



for idx, file in enumerate(files):
    print(file)
    print(idx)
    filename = remove_substring_from_string(file.name,
                                        'Mass2016-VERYTIGHTSCF-VERYTIGHTOPT-')
    filename = remove_substring_from_string(filename, 'Opt-')
    filename = os.path.join(
        'D:\\OwnCloud\\PhD\\calculations\\DenisResearch\\Singlepoint\\B3LYP\\' +
        filename)
    filename = Path('{}-B3LYP-def2TZVPP-SP.inp'.format(os.path.splitext(filename)[0]))
    molecule = Geometry(file)
    molecule.create_Orca_input('!B3LYP def2-TZVPPD SP Mass2016 TIGHTSCF',
                               filename, additional_commands='%pal nprocs 24 end\n%eprnmr\nNuclei = all Cl { aiso, adip, fgrad }\nNuclei = all N { aiso, adip, fgrad }\nend')
"""
    f = open(file)
    data = f.readlines()
    f.close()
    if os.path.exists(filename):
        os.remove(filename)
    with open(filename, 'a') as f:
        for line in data[2:]:
            f.write(line)

files = []
for path in Path('D:\\OwnCloud\\PhD\\calculations\\DenisResearch\\Optimisation\\temp').rglob('*.inp'):
   if '-B3LYP' in path.name:
       files.append(path)
with open('D:\\OwnCloud\\PhD\\calculations\\DenisResearch\\Optimisation\\temp\\def2-TZVPD.txt', 'r+') as basis:
    basislines = basis.readlines()
    for file in files:
        with open(file, 'r+') as f: #r+ does the work of rw
            lines = f.readlines()
            for idx, line in enumerate(lines):
                if '! B1LYP' in line:
                    line = remove_substring_from_string(line, ' def2-TZVPD')
                    lines[idx] = line
            f.seek(0)
            f.writelines(lines)

            f.write('%basis\n')
            for bl in basislines:
                f.write(bl)
            f.write('\n')
            f.write('end\n')


files = []
for path in Path('D:\\OwnCloud\\PhD\\calculations\\DenisResearch\\CCSDT-recalculation').rglob('*.inp'):
   if '-NH3-' in path.name:
       files.append(path)

for file in files:
    with open(file, 'r+') as f: #r+ does the work of rw
        lines = f.readlines()
        for idx, line in enumerate(lines):
            if '%maxcore' in line:
                line = '%maxcore 1000000\n'
                lines[idx] = line
        f.seek(0)
        f.writelines(lines)
"""