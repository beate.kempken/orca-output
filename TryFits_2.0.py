# -*- coding: utf-8 -*-
"""
Created on Tue Jun  7 17:12:49 2022

@author: student
"""
import subprocess
#import bokeh
import copy
import os
import shutil
# import time
import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import curve_fit


def Gaussian(x, amp, mu, sigma):
    return amp / (sigma * np.sqrt(2*math.pi)) * np.exp(-0.5 * ((x-mu)/sigma)**2)


def Lorentzian(x, amp, mu, FWHM):
    return amp/math.pi * 0.5*FWHM / ((x-mu)**2 + (0.5*FWHM)**2)


def tryfitting(cwd, filename, fit_range=5):
    os.chdir(cwd)
    data = np.loadtxt(filename, unpack=True)

    frequency = data[0]
    intensity = data[1]

    Idx = intensity.argmax()
    nu_max = frequency[Idx]

    Gauss_popt, Gauss_pcov = curve_fit(
        Gaussian, frequency[Idx-fit_range:Idx+fit_range], intensity[Idx-fit_range:Idx+fit_range], p0=[0.0001, nu_max, 1])
    rms = np.sqrt(sum((intensity[Idx-fit_range:Idx+fit_range]-Gaussian(frequency[Idx-fit_range:Idx+fit_range], *Gauss_popt))**2))

    x = np.linspace(Gauss_popt[1]-2*Gauss_popt[2],
                    Gauss_popt[1] + 2*Gauss_popt[2])

    print('{:.6f}'.format(Gauss_popt[1]))
    print('rms =', rms)

    plt.plot(frequency, intensity, 'k.')
    plt.plot(x, Gaussian(x, *Gauss_popt))
    plt.show()


#tryfitting('D:\\orca-output-master\\', 'Testdaten3.txt')

'''
x = [1,2,3,4,5]
y = [4,5,3,6,2]    

p = figure(title ='Spectrum', x_axis_label = 'frequency / MHz', y_axis_label = 'Intensity / mV') 

p.line(x, y, legend_label = 'data', line_width = 1)

show(p)
'''


# ~25 datapoints were measured per MHz
def baselinecorrection(cwd, data):
    '''
    Parameters
    ----------
    filename : string
        String consists of name of data file in which the broadband
        spectrum is stored (without extension)       

    Returns
    ----------

    '''

    spectrum = np.loadtxt(cwd + data, delimiter=' ', unpack=True)
    Percentile = []
    p = 70
    Range = 150

    for i in range(len(spectrum[1])):
        if i < Range:
            Percentile.append(np.percentile(spectrum[1][:i+Range], p))
        elif i < len(spectrum[1])-Range:
            Percentile.append(np.percentile(spectrum[1][i-Range:i+Range], p))
        else:
            Percentile.append(np.percentile(spectrum[1][i-Range:], p))

    spectrum[1] = spectrum[1]-Percentile

    plt.plot(spectrum[0], spectrum[1], 'b-', linewidth=0.5)
    plt.xlabel('Frequency / MHz')
    plt.ylabel('Intensity / mV')
    plt.tight_layout()
    plt.show()

    np.savetxt(data[:-4]+'_bl.dat', np.c_[spectrum[0], spectrum[1]], header='frequency in \
    MHz; intensity in mV')

#baselinecorrection('C:\\Users\\beate\\ownCloud\\Research\\furan h2\\Pickett\\','F', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_peaklist.txt','2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0.txt')


def plot_spectrum(cwd, data):
    spectrum = np.loadtxt(cwd + data, unpack=True, skiprows=1)
    #print('Input please!')
    # input()

    fig = plt.figure()
    plt.plot(spectrum[0], spectrum[1], 'k-')
    plt.xlabel('frequency / MHz')
    plt.ylabel('intensity / mV')
    plt.show()

#plot_spectrum('D:\\OwnCloud\\Research\\furan h2\\Pickett\\','2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl.dat')


def blank_spectrum_and_find_peaks(cwd, data, fit_range=5, nu_limit = 0.05, rms_limit=0.1, echofile='echo.asc'):
    '''
    Parameters
    --------
    data: string
        string consists of the name of the file in which the broadband 
        spectrum is stored (with extension)

    threshold: int or float
        minimum intensity value at which a data point can be considered as
        peak and not as noise.

    echofile: string
       string consists of the name of the .asc file which contains the 
       center frequencies of already assigned peaks

    Returns
    --------

    '''

    # load the text file which contains the broadband spectrum
    spectrum = np.loadtxt(cwd + data, unpack=True, skiprows=1)

    '''
    plt.figure()
    plt.plot(spectrum[0],spectrum[1], 'k-')
    plt.xlabel('frequency / MHz')
    plt.ylabel('intensity / mV')
    plt.show()    
    '''

    print('Choose an intensity threshold for peak identification (without unit):')

    threshold = float(input())
    print("Shall already assigned peaks be removed from the baseline-corrected spectrum to create a reduced list of peak frequencies? (answer 'yes' or 'no')")
    Q = input()

    # load the echo.asc file which contains the frequencies of the peaks
    # which have already been assigned
    if Q == 'yes' or Q == 'Yes':
        echo = np.loadtxt(cwd + echofile, usecols=0, unpack=True)

        ####################################################################
        #########     Remove known peaks from spectrum     #################
        ####################################################################

        # create an array which will be filled with the center frequencies
        # of already assigned peaks in the following for-loop
        known_peaks = np.zeros((2, len(echo)), dtype=object)

        for i, known_peak in enumerate(echo):
            Idx = (np.abs(spectrum[0] - known_peak)).argmin()
            known_peaks[0][i] = float(spectrum[0][Idx])
            known_peaks[1][i] = int(Idx)

        # remove remaining zeros from known_peaks array
        p = 0
        while p < len(known_peaks):
            if known_peaks[0][p] == 0:
                known_peaks = np.delete(known_peaks, p, 1)
            else:
                p = p+1

        # set the intensity of data points which are part of a known peak to
        # zero
        for i in range(len(known_peaks[0])):
            j = 0
            k = 0
            while spectrum[1][known_peaks[1][i]-(j+1)] < spectrum[1][known_peaks[1][i]-j]:
                j = j+1
            while spectrum[1][int(known_peaks[1][i]+(k+1))] < spectrum[1][int(known_peaks[1][i]+k)]:
                k = k+1

            spectrum[1][known_peaks[1][i]-j:known_peaks[1][i]+k] = 0
        #np.savetxt(data + '_bl_blanked.txt', np.c_[spectrum[0], spectrum[1]])

    ####################################################################
    ##############    Find peaks in remaining dataset     ##############
    ####################################################################

    # create an array which will be filled with the center frequencies
    # of the unassigned peaks in the spectrum
    peaklist = np.zeros(len(spectrum[0]))
    peak_Intensities = np.zeros(len(spectrum[0]))

    '''
    for i in range(1,len(spectrum[1])-1):
        if spectrum[1][i] > threshold and spectrum[1][i-1] < spectrum[1][i] and spectrum[1][i] > spectrum[1][i+1]:
            
            
            # Intensity-weighted averaging of the three data points with
            # intensities spectrum[1][i-1], spectrum[1][i] and spectrum[1][i+1]
            if spectrum[1][i-1] < spectrum[1][i+1]:

                a_i = spectrum[1][i]-spectrum[1][i-1]
                a = spectrum[1][i+1]-spectrum[1][i-1]
                I_sum = a + a_i
                mu = a_i*spectrum[0][i] / I_sum  +  a*spectrum[0][i+1] / I_sum
                
            else:
                
                a_i = spectrum[1][i]-spectrum[1][i+1]
                a = spectrum[1][i-1]-spectrum[1][i+1]
                I_sum = a + a_i
                mu = a_i*spectrum[0][i] / I_sum  +  a*spectrum[0][i-1] / I_sum
                
            
            peaklist[i] = mu
            peak_Intensities[i] = spectrum[1][i]
    '''
    
    failed_fit = 0
    diff_peak_maxima = []
    failed_fit_rms = []
    failed_frequencies = []
    
    for i in range(1, len(spectrum[1])-1):
        if spectrum[1][i] > threshold and spectrum[1][i-1] < spectrum[1][i] and spectrum[1][i] > spectrum[1][i+1]:
            # print('spectrum[0][i]=',spectrum[0][i])
            
            try:
                popt, pcov = curve_fit(Gaussian, spectrum[0][i-fit_range:i+fit_range], spectrum[1][i-fit_range:i+fit_range], p0=[0.0001,spectrum[0][i],1])
            
            except RuntimeError:
                failed_fit = failed_fit + 1
                failed_frequencies.append(spectrum[0][i])
                
            rms = np.sqrt(sum((spectrum[1][i-fit_range:i+fit_range]-Gaussian(spectrum[0][i-fit_range:i+fit_range], *popt))**2))
            # print('rms = ',rms)
            nu = np.linspace(spectrum[0][i-fit_range],spectrum[0][i+fit_range], 100)            
            
            plt.plot(nu, Gaussian(nu, *popt), 'r-')

            
            if abs(popt[1]-spectrum[0][i]) > nu_limit or rms >= rms_limit:
                failed_fit = failed_fit + 1
                failed_frequencies.append(spectrum[0][i])
                failed_fit_rms.append(rms)
                diff_peak_maxima.append(abs(popt[1]-spectrum[0][i]))

            else:
                peaklist[i] = popt[1]


            # print('popt[1] nach fit = ',popt[1])

            

    peaklist = peaklist[peaklist != 0]
    peak_Intensities = peak_Intensities[peak_Intensities != 0]
    
    plt.plot(spectrum[0], spectrum[1], 'k.')
    plt.show()
    
    print('')
    print('Failed frequencies:')
    print(failed_frequencies)
    print('')
    print('rms of failed fits:')
    print( failed_fit_rms)
    print('')
    print('difference in experimental and fitted peak maxima:')
    print(diff_peak_maxima)
    print('')
    print('Number of failed fits:')
    print(failed_fit)
    print('')
    

    #print("Do you need a plot of the spectrum with the extracted peaks being marked? (answer 'yes' or 'no')")
    #Figure = input()
    #
    # if Figure == 'yes' or Figure == 'Yes':
    
    if Q == 'yes' or Q == 'Yes':
        np.savetxt(cwd + data[:-4] + '_blanked_peaklist' + '_' + str(threshold) + 'mV.txt',
                   peaklist, header='frequency in MHz', fmt='%.6f')
        print('A text file containing the center frequencies of all peaks in the reduced spectrum was created.')

    elif Q == 'no' or Q == 'No':
        np.savetxt(cwd + data[:-4] + '_peaklist' + '_' + str(threshold) + 'mV.txt',
                   peaklist, header='frequency in MHz', fmt='%.6f')
        print('A text file containing the center frequencies of all peaks in the spectrum was created.')

    else:
        print("Spelling 'yes' or 'no' correctly is not that difficult... :P")
    

blank_spectrum_and_find_peaks('D:\\OwnCloud\\Research\\furan h2\\Pickett\\','2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl.dat')

def StartFit(cwd, filename, data, nu1, nu2, nu3, Range, Range_test, RMS_max,
             QN):
    '''

    Parameters
    ----------
    filename : string
        Name of the species (=name of the .par and .lin file)

    data : string
        Name of the data set with extension.

    nu1 : float
        Center frequency around which the program will search for assignments 
        of the 1st transition in the .lin file. (typically theoretically pre-
        dicted frequency)

    nu2 : float
        Center frequency around which the program will search for assignments
        of the 2nd transition in the .lin file (typically theoretically pre-
        dicted frequency)

    nu3 : float
        Center frequency around which the program will search for assignments 
        of the 3rd transition in the .lin file (typically theoretically pre-
        dicted frequency)

    Range : int or float
        Range for assignment search around nu1, nu2 and nu3

    Range_test : int or float
        Range for assignment search around a test transition nu_test (typically
        much smaller than Range)

    QN_i : string
        Quantum numbers for initial state of a test transition with the predicted 
        frequency nu_test

    QN_f : string
        Quantum numbers for final state of a test transition with the predicted 
        frequency nu_test

    RMS_max: int or float
        maximum RMS which can be considered as suitable fit

    Returns
    -------
    a text file with fit results which is saved to the current folder

    Preparation
    -------
    - Save this python file to the folder in which the .cat, .lin, .int and 
      .par file of the species are stored.
    - Create a folder called lin_files and a folder called par_files in the 
      current folder
    - Save a copy of the .par file to the par_files folder and the .lin file 
      with only the three sample assignments to the lin_files folder

    What the program does
    -------
    - The program creates a 1D arrays for each nu. The arrays contain the fre-
      quencies of transitions within the frequency window around the respective
      center frequency.

    - It writes the three assignments to the .lin-file, saves it and runs SPFIT
      and SPCAT.
    - The .par file is restored.
    - The predicted frequency nu_test of a test transition is extracted from 
      the .cat file.
    - A 1D array is created with peak frequencies in the spectral window around
      nu_test. Only one peak at lower frequency than nu_test and on at higher 
      frequency than nu_test will be used (those which are closest to nu_test). 
    - The respective frequencies are assigned to the test transition QN_f <- QN_i 
      (one after the other). 
    - SPFIT is run with the four assignments
    - If the resulting RMS is smaller than RMS_max, the fit result is stored to a 
      .txt file
    - The next tuple of 3 assignments is created, the whole procedure repeats.
    '''

    # tell the system the current path so that spfit and spcat know where to be run
    os.chdir(cwd)

    # make lin_files and par_files folder
    if 'lin_files' and 'par_files' not in os.listdir(cwd):
        os.mkdir(cwd + 'lin_files')
        os.mkdir(cwd + 'par_files')

    shutil.copy(cwd + filename + '.par', cwd +
                'par_files\\' + filename + '.par')
    shutil.copy(cwd + filename + '.lin', cwd +
                'lin_files\\' + filename + '.lin')

    # print('I made folders. :)')

    # load the broadband spectrum
    peaklist = np.loadtxt(cwd + data, unpack=True)

    # define 1D arrays with peak frequencies in the spectral window around
    # predictions nu1, nu2 and nu3
    peaks_1 = np.array(
        [x for x in peaklist if x > nu1 - Range and x < nu1 + Range])
    peaks_2 = np.array(
        [x for x in peaklist if x > nu2 - Range and x < nu2 + Range])
    peaks_3 = np.array(
        [x for x in peaklist if x > nu3 - Range and x < nu3 + Range])

    # definition of an empty array which will be filled with the fit results at a
    # later point
    best_results = np.zeros((100, 7+len(QN)))

    cmd = ['spfit', filename + '.lin', filename + '.par']
    cmd2 = ['spcat', filename + '.var', filename + '.par', filename + '.int']

    print('lengths of the peaklists peaks_1, peaks_2 and peaks_3:', len(peaks_1),
          len(peaks_2), len(peaks_3))

    # counter for the lines in the result file (always increases by 1 as soon as
    # a new line has been added)
    x = 0

    # open the backup .lin file and store its content in
    lin_in = open(cwd + 'lin_files\\'+filename + '.lin', 'r')

    lin_original = []
    for line in lin_in.readlines():
        values = line.split()
        lin_original.append(values)
    lin_in.close()

    # iterate through all combinations of assignments which are possible with the
    # frequencies in peaks_1, peaks_2 and peaks_3
    for i in peaks_1:
        print('i=', i)
        for j in peaks_2:
            for k in peaks_3:
                check = 0
                # Write the new assignment to the .lin file
                counters = np.array([i, j, k])

                lin = copy.deepcopy(lin_original)

                for y in range(len(lin_original)):   # should contain 3 lines
                    tmp = 7-len(str(int(counters[y])))
                    whitespace = ' '*tmp
                    lin[y][0] = ' '*2 + lin_original[y][0]
                    lin[y][-3] = whitespace + str('{:.6f}'.format(counters[y]))
                    lin[y][-2] = ' '*3 + lin_original[y][-2]
                    lin[y][-1] = ' ' + lin_original[y][-1]

                # save the updated .lin file
                np.savetxt(cwd + filename + '.lin', lin,
                           fmt='%s', delimiter='  ')

                # Run SPFIT and SPCAT and restore .par-file
                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                subprocess.run(cmd2, stdout=subprocess.PIPE, cwd=cwd)
                shutil.copy(cwd + 'par_files\\' + filename +
                            '.par', cwd + filename + '.par')

                # open .cat file and save the frequency of the test transitions
                # QN_f <- QN_i in nu_test list

                nu_test = []

                for v in range(len(QN)):
                    cat = open(cwd + filename + '.cat', 'r')
                    for line in cat.readlines():
                        if QN[v][0] in line and QN[v][1] in line:
                            nu_test.append(float(line[:13]))
                    cat.close()

                # define 1D array which contains frequencies of peaks in the
                # test frequency window of range_test around nu_test
                nu_test_exp = []

                for v in range(len(nu_test)):
                    window = np.array(
                        [i for i in peaklist if abs(nu_test[v] - i) < Range_test])
                    if len(window) > 0:
                        Idx = np.abs(window - nu_test[v]).argmin()
                        nu_test_exp.append(window[Idx])

                # if no transition is found, the fit is wrong and we can pass to another combination.
                    else:
                        check = 1
                        break

                if check == 1:
                    continue

                for v in range(len(nu_test_exp)):
                    digits = len(str(int(nu_test_exp[v])))

                    with open(cwd + filename + '.lin', 'a+') as lin_mod:
                        lin_mod.write('  ' + QN[v][0][0]+'  ' + QN[v][0][2] + '  '
                                      + QN[v][0][4] + '  ' + QN[v][1][0] + '  ' + QN[v][1][2] +
                                      '  ' + QN[v][1][4] +
                                      '  0  0  0  0  0  0' + (9-digits)*' '
                                      + str(nu_test_exp[v]) + '     0.010000   1.00000' + '\n')

                subprocess.run(cmd, stdout=subprocess.PIPE, cwd=cwd)
                shutil.copy(cwd + 'par_files\\' + filename +
                            '.par', cwd + filename + '.par')
                # print('stop')
                # time.sleep(10)
                np.savetxt(cwd + filename + '.lin', lin,
                           fmt='%s', delimiter='  ')

                # read .fit-file: extract RMS, A, B and C
                with open(cwd + filename + '.fit', 'r') as f:
                    fitfile = f.readlines()

                    RMS = [float(line[16:32]) for line in fitfile
                           if line.startswith(' MICROWAVE RMS =')]
                    RMS = min(RMS)  # only the last RMS value is relevant
                    # as it is the one at which the RMS has
                    # converged to a minimum value

                    A = [float(line[29:46]) for line in fitfile
                         if '   10000          A    ' in line]
                    A = A[-1]
                    B = [float(line[29:46]) for line in fitfile
                         if '   20000          B    ' in line]
                    B = B[-1]
                    C = [float(line[29:46]) for line in fitfile
                         if '   30000          C    ' in line]
                    C = C[-1]

                # extract fits with low RMS and write their results
                # to a _fit_results.txt file

                frequencies = [i, j, k] + nu_test_exp

                if RMS < RMS_max and A > 0 and B > 0 and C > 0 and len(frequencies) == len(set(frequencies)):
                    for v in range(len(nu_test_exp)):
                        best_results[x][0] = RMS
                        best_results[x][1] = i
                        best_results[x][2] = j
                        best_results[x][3] = k
                        best_results[x][4+v] = nu_test_exp[v]
                        best_results[x][-3] = A
                        best_results[x][-2] = B
                        best_results[x][-1] = C

                    x = x+1
                    np.savetxt(cwd + filename + '_fit_results.txt',
                               best_results, header='RMS / MHz    nu_1 / MHz\
                    nu_2 / MHz    nu_3 / MHz    nu_test / MHz   \
                    A / MHz    B / MHz    C / MHz',
                               fmt='%.6f, %.6f, %.6f, %.6f,' + len(nu_test_exp) * ' %.6f,' + ' %.5f, %.5f, %.5f')
                    print('I found something! :)')

    shutil.copy(cwd + 'lin_files\\' + filename +
                '.lin', cwd + filename + '.lin')

    shutil.rmtree(cwd + 'lin_files')
    shutil.rmtree(cwd + 'par_files')


#StartFit('3-c-FBz-H2-2', '3FBz_H2_3barAbs_100000_0.dat', 4975.153547, 5172.081910, 7120.548172, 80, 5, 0.01, '3 1 3' , '2 0 2')
#StartFit('3-t-FBz-H2-2', '3FBz_H2_3barAbs_100000_0.dat', 5413.509143, 6009.173712, 7491.767000, 80, 5, 0.003, '4 1 4' , '3 1 3')
#StartFit('4-FBz-H2-O-bound', '4FBz-H2_23052022_25C-3barNe_23052022_2__3900k_FT_avg_bl_blanked_peaklist.txt', 4923.38720, 5127, 5363, 80, 5, 0.01, '4 0 4' , '3 0 3')
#StartFit('F_C1C4', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_blanked_peaklist.txt', 23007, 23136, 23156, 50, 5, 0.01, '4 2 2' , '4 2 3')
#StartFit('C:\\Users\\beate\\ownCloud\\Research\\furan h2\\Pickett\\','F', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_peaklist.txt', 23249, 23443, 23430, 20, 5, 0.004, [['4 2 2' , '4 2 3'], ['5 3 2' , '5 3 3'], ['4 3 2', '4 1 3']])
#StartFit('C:\\Users\\beate\\ownCloud\\Research\\furan h2\\Pickett\\','F_C1C4', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_peaklist.txt', 23007, 23136, 23156, 120, 5, 0.01, [['4 2 2' , '4 2 3'], ['5 3 2' , '5 3 3'], ['3 2 2', '3 0 3']])
# StartFit('D:\\OwnCloud\\Research\\furan h2\\Pickett\\','F-(o-H2)', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_blanked_peaklist.txt', 20920, 21020, 25243, 500, 5, 0.07, [['5 1 4' , '5 1 5'], ['6 1 5', '6 1 6']])
# StartFit('D:\\OwnCloud\\Research\\furan h2\\Pickett\\','F-H2O_O-bound', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_blanked_peaklist.txt', 24828, 24190, 23004, 300, 5, 1, [['7 1 7', '6 1 6'], ['6 1 5', '5 1 4'], ['6 0 6', '5 0 5']])
# StartFit('D:\\OwnCloud\\Research\\furan h2\\Pickett\\','F', '2019-07-12_1705430652_Fu_H2_28bar_Ne_RT_PXI1-0-0-INSTR_Channel1_Segment1_0.5MaveragedTD_FT_0_bl_peaklist.txt', 23249, 23443, 23435, 20, 5, 0.1, [['4 2 2' , '4 2 3'], ['5 3 2' , '5 3 3'], ['4 3 2', '4 1 3'], ['3 2 2', '3 0 3']])
