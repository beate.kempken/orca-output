# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 14:10:49 2021

@author: Dohmen
"""

# Import the necessary packages
import sys
import numpy as np

from pathlib import Path

"""
Add git repository path for orca_coord to path for work computer
Your personal Path to the orca-output folder needs to be put here. The way
you need to handle the '\' depends on your OS.
"""
sys.path.insert(0, 'D:\\orca-output')

# add git repository path for orca_coord to path for home computer
sys.path.insert(0, 'S:\\PhD\\orca-output')

# Import important functions from complementary files
from GeometryClass import Geometry, information_to_excel
from spfitExtension import (read_multiple_linfiles,
                                       search_for_duplicates,
                                       same_qantum_numbers)
from crestExtension import crest_xyz_to_ORCA
from strfitExtension import submit_multipe_stf_files

###########################################################################

"Print information of multiple files to an excel sheet"

###########################################################################
"""
Search for regular orca output files in the folder you want to look at
sometimes this search grabs files which aren't really the ORCA output files
like the _trj.out files, files created by the cluster schedulers and others.
These need to be excluded.
In this example each Path is appended to a list for further use. A few files
need to be excluded such as the slurm ouput files, atom and _trj files which
are created by orca
"""

files = []
for path in Path(
    'D:\\ownCloud\\Obenchain\\Research\\furan h2\\orca').rglob('*.out'):
    if '_trj' not in path.name:
        if 'slurm' not in path.name:
            if 'atom' not in path.name:
                    files.append(path.resolve())

"""
Execute the script which reads the files, does the transformation and prints
it to excel. If not otherwise specified, it will print it into the first
folder in the files list. Another folder can be specified, otherwise it gets
saved in the first one folder in the file list.
"""

df = information_to_excel(files,
              outputfile='D:\\ownCloud\\Obenchain\\Research\\furan h2\\orca\\results_table.xlsx')


###########################################################################

"Print a shortened output file with transformed information for fitting with"
"Pickett or Pgopher"

###########################################################################

molecule = Geometry('S:\\PhD\\Owncloud2\\Obenchain\\Research\\boronic-esters\\PhB-pinacol-ester\\orca\\H2-complex\\PhB-pinacol-ester-H2-B3LYP-D3-def2-TZVPPD_2\\PhB-pinacol-ester-H2-B3LYP-D3-def2-TZVPPD_2.out')
# molecule.format_output()


###########################################################################

"Create an ORCA input file containing coordinates in the PSA"

###########################################################################
"""
Create an xyz file with the coordinates in the prinicpal axis system. By
default it is saved in the same folder, but the file can be specified.
The order of the atoms can also be changed. It needs to be a list of integers
with the current atom labels in the new order. (see documentation)
"""

# molecule.create_xyz_file(outputfile='my_molecule.xyz')


###########################################################################

"Create an ORCA input file containing coordinates in the PSA"

###########################################################################

# molecule.create_Orca_input('! B3LYP D3 def2-TZVPP Opt Mass2016',
#                           comment='my molecule',
#                           additional_commands='%eprnmr Nuclei = all Cl' +
#                                          '{ aiso, adip, fgrad }\n       end',
#                           outputfile='my-calculation')


###########################################################################

"Create internal coordinate file for strfit"

###########################################################################
"""
Define a connection matrix for the internal coordinates that is to your liking
for fitting the parameters you need. It needs the atom labels for the
connecting atom in the first column, the third atom to form an angle in the
second column and the third atom for the formation of an dihedral angle in the
third column. It gets printed as ususal.
"""
connection_matrix = np.array([[0, 0, 0],
                              [1, 0, 0],
                              [2, 1, 0],
                              [3, 2, 1],
                              [1, 2, 3],
                              [1, 2, 3],
                              [2, 1, 5],
                              [3, 2, 1],
                              [4, 3, 2],
                              [4, 3, 2]])

# molecule.create_interal_coordinates(connection_matrix)


###########################################################################

"Compare multiple lin files from pickett, seaching for frequencies which were"
"assingend multiple times in different lin files"

###########################################################################
"""
Read all .lin files in directory and it's subdirectories. These are put in a
dictionay which can be given to multiple functions for different comparisons.
Search_for_duplicates shows all frequencies which were doubly assigned in
several .lin files.

Same_quantum_numbers looks for transitions with the same quantum numbers which
were assigned in both files eg. for the purpose of determing relative
populations.
"""

#lin_dict = read_multiple_linfiles(
#    'S:\\PhD\\Owncloud2\\PhD\\assignment\\Benzaldehydes\\4-chlorobenzaldehyde')

#doubles = search_for_duplicates(lin_dict)
#qn = same_qantum_numbers(lin_dict, '4-Cl35', '4-13C6')


###########################################################################

"Convert crest confomers to ORCA input"

###########################################################################
"""
Function converts crest confomers in a given folder into input files for ORCA.
The function reads the energy of the confomer in the folder to let the user
determine a cutoff value or a number of low energy confomers to use. The
geometry is read and an input file created for each confomer in a new folder.
A default command line with the ORCA command
!B3LYP D3 def2-TZVP VeryTightOpt VeryTightSCF Mass2016
is used but can be changed beforehand when calling the function.
"""

# folder = 'D:\\OwnCloud\\PhD\\calculations\\fluorobenzaldehydes\\4-FBz\\crest\\H2-3'
# crest_xyz_to_ORCA(folder, additonal_commands='%pal nprocs 24 end')


###########################################################################

"Create a strfit file from template"

###########################################################################
"""
This function is primarily designed for submiting a number of strfit jobs using
different geometries as starting guesses. An strfitfile needs to be specified
which contains the desired fitting parameters.
The starting guesses should be xyz files or orca ouput files with a geometry
of the same molecule. The function reads each geometry, calculates the internal
coordinates based on the ones used in the template. Therefore the order of the
atom and the geometry file should be the same or the correct order conversion
to the strfit atom order needs to be supplied. Based on the template a strfit
file with the same fitting parameters is created and submitted.
The function 'information to excel' can also recognize a list of strfit outputs
and put these into an excel sheet.
"""
# strfitfile = 'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer\\B2PLYP-D3-def2-TZVPP\\01-4-ClBz-strfit-6.stf'

# files = []
# for path in Path(
#              'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer').rglob('*.out'):
#     if '_trj' not in path.name:
#         if 'slurm' not in path.name:
#             if 'SP' not in path.name:
#                 if 'strfit' not in path.name:
#                     files.append(path.resolve())

# submit_multipe_stf_files(strfitfile, files, [11, 12, 6, 3, 2, 0, 1, 5, 4, 13, 7, 8, 9, 10])

# files = []
# for path in Path(
#         'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer\\').rglob('*.out'):
#     if 'strfit-' not in path.name:
#        if 'strfit' in path.name:
#             files.append(path.resolve())

# information_to_excel(files, 'D:\\OwnCloud\\PhD\\calculations\\chlorobenzaldehydes\\4-ClBz\\monomer\\strf_restult.xlsx')

